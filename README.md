# Component: Backend BOM

The component is Bill of Materials for all backend components. This is made for keeping consistent versions of each
component along all backend services.

## Packaging

pom

## Description

The project consists versions which should be used of many libraries but the most important are:

- spring-boot-dependencies
- spring-cloud-dependencies
- springdoc-openapi-webflux-ui
- mongock-spring
- mockserver-netty
- mockserver-client-java
- guava
- lombok
- mapstruct
- jetty-reactive-httpclient

## Creating local release

```
mvn clean install
```

## Maven

If you want to use it just add the 'dependencyManagement' section in your pom.xml file. It is already included in
backend-web-parent project.

```
    <properties>
        <backend-bom.version>1.0.5-SNAPSHOT</backend-bom.version>
    </properties>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>com.jkarkoszka.getjabbed</groupId>
                <artifactId>backend-bom</artifactId>
                <version>${backend-bom.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>
```

## Access to GetJabbed Maven Repository

### settings.xml

```
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0
                      http://maven.apache.org/xsd/settings-1.0.0.xsd">

    <activeProfiles>
        <activeProfile>main</activeProfile>
    </activeProfiles>

    <profiles>
        <profile>
            <id>main</id>
            <repositories>
                <repository>
                    <id>central</id>
                    <url>https://repo1.maven.org/maven2</url>
                </repository>
                <repository>
                    <id>gitlab-maven</id>
                    <url>https://gitlab.com/api/v4/groups/51789812/-/packages/maven</url>
                </repository>
            </repositories>
        </profile>
    </profiles>

    <servers>
        <server>
            <id>gitlab-maven</id>
            <configuration>
                <httpHeaders>
                    <property>
                        <name>Private-Token</name>
                        <value>###PUT HERE PERSONAL ACCESS TOKEN###</value>
                    </property>
                </httpHeaders>
            </configuration>
        </server>
    </servers>
</settings>
```

You can generate personal access token here: https://gitlab.com/-/profile/personal_access_tokens